$.smartskuPluginProductElements._Selectors.sku_options = '.item-sidebar__options';
$.smartskuPluginProductElements._Selectors.sku_feature_container = '.options__buttons';
$.smartskuPluginProductElements._Selectors.price = '.cat-item__price-amount';
$.smartskuPluginProductElements._Selectors.sku_feature_button = 'li';
$.smartskuPluginProductElements._Selectors.form_action_indicator = 'id';
smartskuPlugin.init =  function () {
    setInterval(function(){
        $('input[name="product_id"]').each(function() {
            var input = $(this);
            var form = input.closest('form');
            var indicator = false;
            if(form) {
                var indicator = form.attr($.smartskuPluginProductElements.Selectors().form_action_indicator);
            }
            if(indicator && !form.find('.smartsku-id').hasClass('smartsku-id') && !form.find('.salesku-id').hasClass('salesku-id')) {
                input.after('<span class="smartsku-id"></span>');
                $.get(shop_smartsku_wa_url+'smartsku_plugin_product/'+input.val()+'/',function (response) {
                    input.after(response);
                });
            }
        });
    },100);
}; 