$.smartskuPluginProductElements._Selectors.sku_feature_container = '.feature-button';
$.smartskuPluginProductElements._Selectors.sku_feature_button = '.at-stylize-label, label';

smartskuPluginProductSkuFeature.prototype.getId = function() {
    return this.getElement().data(this.Selectors().sku_feature_element_data_id);
};

smartskuPluginProductSkuFeature.prototype.getButtons = function () {
    return this.getElement()
        .closest(this.Selectors().sku_feature_container)
        .find(this.Selectors().sku_feature_button);
};
smartskuPluginProductSkuFeature.prototype.getButtonValue = function($button_obj) {
    return  $button_obj.find('input').val();
    };
smartskuPluginProductSkuFeature.prototype.setButtonState = function(button, state) {
    var self = this;
    if(state===1) {
        button
            .addClass(self.Selectors().sku_feature_value_class_active)
            .removeClass(self.getHideClass(1))
            .removeClass(self.getHideClass(2));
        button.find('span') .addClass(self.Selectors().sku_feature_value_class_active);
    } else if(state===0) {
        button.removeClass(self.Selectors().sku_feature_value_class_active);
        button.find('span') .removeClass(self.Selectors().sku_feature_value_class_active);
    }
};


