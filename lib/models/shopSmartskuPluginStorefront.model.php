<?php
class shopSmartskuPluginStorefrontModel extends waModel
{
    protected $table = 'shop_smartsku_storefront';

    public function getByStorefrontName($name) {
        return $this->getByField('name', $name);
    }
}

