<?php

class shopSmartskuPluginBackendCreateFilesController extends waJsonController {
    
    public function execute() {
        $storefront = waRequest::get('storefront');
        if(empty($storefront)) {
            $storefront = shopSmartskuPlugin::GENERAL_STOREFRONT;
        }
        if($storefront == shopSmartskuPlugin::GENERAL_STOREFRONT) {
            return;
        }
        $settings = shopSmartskuPlugin::getPluginSettings($storefront);
        $storefront = $settings->getStorefront();
        $templates = new shopSmartskuPluginTemplates($settings);
        foreach ($storefront->getThemes() as $type => $theme) {
            if($theme) {
                foreach ($templates->getThemeTemplates() as $k => $name) {
                    $theme->addFile($name, '');
                    $theme->save();
                    $this->logAction('template_add', $name);
                    $content = $templates->getTemplatePluginContent($k);
                    $file_path = $theme->getPath().'/'.$name;
                    if (!file_exists($file_path)) {
                       @file_put_contents($file_path, $content);
                    } 
                }
            }
        }
    }
}
