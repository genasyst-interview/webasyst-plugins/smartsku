<?php

class shopSmartskuPluginBackendSettingsAction extends waViewAction {
    
    public function execute() {
        $storefront = waRequest::get('storefront');
        if(empty($storefront)) {
            $storefront = shopSmartskuPlugin::GENERAL_STOREFRONT;
        }
        $settings = shopSmartskuPlugin::getPluginSettings($storefront);
        if(waRequest::method()=='get') {
            $this->view->assign('settings', $settings);
            $this->view->assign('image_sizes', $settings->getImageSizes());
            $this->view->assign('storefront', $settings->getStorefront());
        } else {
            $data = waRequest::post();
            $settings->save($data);
        }
    }
}
