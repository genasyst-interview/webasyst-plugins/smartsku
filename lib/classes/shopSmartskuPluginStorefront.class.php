<?php
class shopSmartskuPluginStorefront {
    protected $name = 'general';
    protected $id = 0;
    public function __construct($storefront = null)  {
        $this->setStorefront($storefront);
    }
    protected function setStorefront($storefront = null) {
        $model = new shopSmartskuPluginStorefrontModel();
        $storefront_data = $model->getByStorefrontName($storefront);
        if(!empty($storefront_data)) {
            $this->name = $storefront_data['name'];
            $this->id = $storefront_data['id'];
        } else {
            $id = $model->insert(array('name' => $storefront));
            if(!empty($id)) {
                $this->name = $storefront;
                $this->id = $id;
            }
        }
    }
    public function getThemes() {
            $storefront_data = self::splitUrl($this->getName());
            if($storefront_data) {
                $routing = wa()->getRouting()->getRoutes($storefront_data['domain']);
                foreach ($routing as $route) {
                    if($route['app'] == shopSmartskuPlugin::APP && $route['url'] == ltrim($storefront_data['url'], '/\\'))  {
                        $theme = new waTheme($route['theme'], shopSmartskuPlugin::APP);
                        $theme_mobile = ($route['theme'] == $route['theme_mobile'])? false : new waTheme($route['theme_mobile'], shopSmartskuPlugin::APP);
                        return array(
                            'theme' => $theme,
                            'theme_mobile' => $theme_mobile,
                        );
                    }
                }
            }
        return array();
    }
    public static function splitUrl($url)
    {
        if(preg_match('@^(?:http://|https://)?([^/]+)([\/].*)?@i', mb_strtolower($url), $url_arr)) {
            if(count($url_arr)==3) {
                return  array(
                    'domain' => $url_arr[1],
                    'url' => $url_arr[2]
                );
            }
        }
        return false;
    }
    public function getId(){
        return $this->id;
    }
    public function getCode() {
        return md5($this->name);
    }
    public function getName() {
        return $this->name;
    }
    public function __toString()
    {
        return (string)$this->getName();
    }
}